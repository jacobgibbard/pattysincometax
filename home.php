<?php
/*
Template Name: Homepage
*/
get_header(); ?>

<?php echo do_shortcode('[orbit-slider]') ?>

<div class="cta-bg">
<div class="row">
	<div class="small-12 large-3 columns">
		<a href="/services/personal-tax-services/">
			<div class="cta-box box-1">
				<div class="icon-wrap">
					<span class="glyphicon fa fa-info-circle"></span>
				</div>
				<h3>Personal Services</h3>
				<h4>We can help get you started on the road to recovery</h4>
				<div class="clearfix"></div>
			</div>
		</a>
	</div>
	<div class="small-12 large-3 columns">
		<a href="/services/business-tax-services/">
			<div class="cta-box box-2">
				<div class="icon-wrap">
					<span class="glyphicon fa fa-building"></span>
				</div>
				<h3>Business Services</h3>
				<h4>Serving Small Business' to Large Corporations</h4>
				<div class="clearfix"></div>
			</div>
		</a>
	</div>
	<div class="small-12 large-3 columns">
		<a href="/category/tax-news/">
			<div class="cta-box box-3">
				<div class="icon-wrap">
					<span class="glyphicon fa fa-hacker-news"></span>
				</div>
				<h3>Tax News</h3>
				<h4>Up to date tax news and resources to help you learn</h4>
				<div class="clearfix"></div>
			</div>
		</a>
	</div>
	<div class="small-12 large-3 columns">
		<a href="/contact/">
			<div class="cta-box box-4">
				<div class="icon-wrap">
					<span class="glyphicon fa fa-users"></span>
				</div>
				<h3>Get In Touch</h3>
				<h4>Find out why we've helped so many people today</h4>
				<div class="clearfix"></div>
			</div>
		</a>
	</div>
</div>
</div>

<div class="row">
	<div class="small-12 large-12 columns" role="main">

	<?php /* Start loop */ ?>
	<?php while (have_posts()) : the_post(); ?>
		<article <?php post_class() ?> id="post-<?php the_ID(); ?>">
			<div class="entry-content">
				<?php the_content(); ?>
			</div>
			<footer>
				<?php wp_link_pages(array('before' => '<nav id="page-nav"><p>' . __('Pages:', 'FoundationPress'), 'after' => '</p></nav>' )); ?>
				<p><?php the_tags(); ?></p>
			</footer>
			<?php comments_template(); ?>
		</article>
	<?php endwhile; // End the loop ?>

	</div>
</div>

<?php get_footer(); ?>
