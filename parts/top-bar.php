<div class="top-bar-container contain-to-grid show-for-medium-up">
    <nav class="top-bar" data-topbar role="navigation">
        <ul class="title-area">
            <li class="name">
                <a href="/"><img src="/wp-content/uploads/2015/03/finallogo.png" width="140" /></a>
            </li>
        </ul>
        <section class="top-bar-section">
            <?php foundationPress_top_bar_l(); ?>
            <?php foundationPress_top_bar_r(); ?>
            <?php echo do_shortcode('[google-translator]'); ?>
        </section>
    </nav>
</div>
