<div class="row">
 <div class="columns large-12">
 	<?php echo do_shortcode('[show_testimonials]') ?>
  </div>
</div>
</section>
	</div>
</div>
<footer id="footer">
	<div class="row">
		<?php do_action('foundationPress_before_footer'); ?>
		<?php dynamic_sidebar("footer-widgets"); ?>
		<?php do_action('foundationPress_after_footer'); ?>
	</div>
</footer>
<a class="exit-off-canvas"></a>

	<?php do_action('foundationPress_layout_end'); ?>

<?php wp_footer(); ?>
<?php do_action('foundationPress_before_closing_body'); ?>
</body>
</html>
